import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutPageComponent} from './about-page/about-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TaskPageComponent } from './task-page/task-page.component';
 
//Routes for the website. LandingPage is the homepage. 
const routes: Routes = [
{ path: '', component: LandingPageComponent },
{ path: 'about', component: AboutPageComponent},
{ path: 'task', component: TaskPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
